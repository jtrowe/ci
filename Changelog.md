# Changelog

## 0.4.3 - 2024-12-19

- Fix scripts
  [#17](https://gitlab.com/jtrowe/docker-buildtools/-/issues/17)
  - Rework gitlab CI.
  - Rename bin/build to bin/fake-build.
  - Add bin/build-simple-release-doc.
- Release 0.4.3
  [#19](https://gitlab.com/jtrowe/docker-buildtools/-/issues/19)
- Fix CI
  [#20](https://gitlab.com/jtrowe/docker-buildtools/-/issues/20)
- build-simple-release-doc might not have docker image list
  [#21](https://gitlab.com/jtrowe/docker-buildtools/-/issues/21)


## 0.4.2 - 2024-11-17

- Add support for building Docker images
  [#8](https://gitlab.com/jtrowe/docker-buildtools/-/issues/8)
- Release 0.4.2
  [#9](https://gitlab.com/jtrowe/docker-buildtools/-/issues/9)


## 0.4.1 - 2024-11-16

- Attach build artifacts to release
  [#4](https://gitlab.com/jtrowe/docker-buildtools/-/issues/4)
- Using job dependencies incorrectly
  [#6](https://gitlab.com/jtrowe/docker-buildtools/-/issues/6)
- Release 0.4.1
  [#7](https://gitlab.com/jtrowe/docker-buildtools/-/issues/7)


## 0.4.0 - 2024-11-15

- Add README
  [#1](https://gitlab.com/jtrowe/docker-buildtools/-/issues/1)
- Prototype GitLab CI
  [#2](https://gitlab.com/jtrowe/docker-buildtools/-/issues/2)
