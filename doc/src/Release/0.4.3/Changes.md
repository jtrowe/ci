- Fix scripts
  [#17](https://gitlab.com/jtrowe/docker-buildtools/-/issues/17)
  - Rework gitlab CI.
  - Rename bin/build to bin/fake-build.
  - Add bin/build-simple-release-doc.
- Release 0.4.3
  [#19](https://gitlab.com/jtrowe/docker-buildtools/-/issues/19)
- Fix CI
  [#20](https://gitlab.com/jtrowe/docker-buildtools/-/issues/20)
- build-simple-release-doc might not have docker image list
  [#21](https://gitlab.com/jtrowe/docker-buildtools/-/issues/21)
