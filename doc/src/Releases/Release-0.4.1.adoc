= Release 0.4.1-rc1

:doctype: article
:revdate: 2024-11-15
:revnumber: 0.4.1


This is the new 0.4.1-rc1 release of the jtrowe/ci project.

