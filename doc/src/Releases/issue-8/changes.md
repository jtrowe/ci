*   Calculate lib_dir from $0 when loading bash library
*   Assume all commands in script will be found on the $PATH
*   Add helper scripts from jtrowe/Docker-buildtools
    *   bin/docker-build
    *   bin/docker-image-created
    *   bin/docker-image-id
    *   bin/docker-inspect
    *   bin/docker-push
    *   bin/git-current-commit
    *   bin/install-debian-packages
    *   bin/set-modified-time
