*   Attach packages files to the release.
*   Set job dependencies in gitlab-ci.
*   Add RELEASE_NOTES_FILE in .project-vars.yml. <br>
    Will appear as the text of the release in gitlab release pages.
*   Add lib/bash/check_command.bash Bash library.
*   Add bin/detect-os script.
*   bin/build changes:
*   *   Detect the os.
*   *   Add logic (turned off right now) to show more environment.
*   Add release process documentation.
