
function check_command() {
    cmd="$1"

    echo -n "    checking for $cmd : "

    result="$(which $cmd)"

    if [ 0 == $? ]
    then
        echo -n "$result"
    else
        echo -n "missing"
    fi

    echo ""

    eval "export has_${cmd}=$result"
}
